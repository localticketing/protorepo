#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PUSH=$1

# Helper for adding a directory to the stack and echoing the result
function enterDir {
  # echo "Entering $1"
  pushd $1 > /dev/null
}

# Helper for popping a directory off the stack and echoing the result
function leaveDir {
  # echo "Leaving `pwd`"
  enterDir $DIR
}

function generateProtos {
    if [ "${PUSH:-nopush}" == "push" ]; then
        docker run -v `pwd`:/work uber/prototool prototool generate
    else
        prototool generate
    fi
}


function pushProtorepoPhp {
    enterDir "gen/protorepo-php"
    # rm -rf clone
    enterDir "Localticketing"
    
    for d in */; do
        mkdir "../${d}" > /dev/null 2>&1
        rm -rf "../${d}src"
        mv $d "../${d}src"

        rm -rf "../${d}metadata"
        mv "../GPBMetadata/${d}" "../${d}metadata"
    done

    leaveDir
    enterDir "gen/protorepo-php"
    
    rm -rf Localticketing
    rm -rf GPBMetadata
    
    leaveDir
}

function cloneProtorepos {
    mkdir "gen"
    enterDir "gen"
    
    git clone git@bitbucket.org:localticketing/protorepo-php.git
    git clone git@bitbucket.org:localticketing/protorepo-go.git
    git clone git@bitbucket.org:localticketing/protorepo-js.git
    git clone git@bitbucket.org:localticketing/protorepo-cpp.git
    
    leaveDir
}

function commit {
    enterDir "gen"

    git config --global user.email "info@localticketing.de"
    git config --global user.name "localTicketing Bot"

    for d in */; do
        enterDir $d
        git add -N .

        if ! git diff --exit-code > /dev/null; then
            git add .
            git commit -m "Auto Creation of Proto"
            git push origin HEAD
        else
            echo "No changes detected for $d"
        fi
        enterDir ..
    done
}

function build {
    if [ "${PUSH:-nopush}" == "push" ]; then
        cloneProtorepos
    fi
    
    generateProtos
    pushProtorepoPhp
    
    if [ "${PUSH:-nopush}" == "push" ]; then
        commit
    fi
}

build